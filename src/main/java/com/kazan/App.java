package com.kazan;

import com.kazan.view.MainView;

public class App {

    public static void main(String[] args) {
        new MainView().show();
    }
}
