package com.kazan.model;

import com.kazan.model.patterns.Patterns;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Text {

    private String inputText;

    private List<Sentence> sentences;

    public Text(String fileMame) {

        try {
            inputText = new String(Files.readAllBytes(Paths.get(fileMame)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sentences = new ArrayList<>();
    }

    public List<Sentence> getSentences() {
        Pattern pattern = Patterns.SENTENCE_PATTERN;
        Matcher matcher = pattern.matcher(inputText);
        while (matcher.find()) {
            sentences.add(new Sentence(inputText.substring(matcher.start(), matcher.end())));
        }
        return sentences.stream()
                .filter(sentence -> !sentence.getSentenceText().isEmpty())
                .collect(Collectors.toList());
    }

    public String getInputText() {
        return inputText;
    }
}
