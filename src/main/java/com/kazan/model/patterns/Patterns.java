package com.kazan.model.patterns;

import java.util.regex.Pattern;

public class Patterns {

    public static final Pattern SENTENCE_PATTERN = Pattern.compile("[^.!?]*[.!?]");
    public static final Pattern WORD_PATTERN = Pattern.compile("\\b[a-zA-Z']+\\b");
    public static final Pattern QUESTION_PATTERN = Pattern.compile(".*\\?");
}
