package com.kazan.controller;

import com.kazan.model.Sentence;
import com.kazan.model.Text;
import com.kazan.model.TextAnalyze;
import com.kazan.model.Word;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

public class ControllerImpl implements Controller {

    private Text text;
    private TextAnalyze textAnalyze;

    public ControllerImpl() {
        text = new Text("src/main/resources/text.txt");
        textAnalyze = new TextAnalyze(text);
    }

    @Override
    public Map<String, Integer> doTask1() {
        return textAnalyze.findLargestNumberOfSentencesWithSameWords();
    }

    @Override
    public Map<Sentence, Integer> doTask2() {
        return textAnalyze.outputSentencesIncreasingWordsNumber();
    }

    @Override
    public void doTask3() {

    }

    @Override
    public List<Word> doTask4(int length) {
        return textAnalyze.outputQuestionWordsOfGivenLength(length);
    }

    @Override
    public void doTask5() {

    }

    @Override
    public List<String> doTask6() {
        return textAnalyze.printoutSortedWords();
    }

    @Override
    public Map<Word, Double> doTask7() {
        Map<Word, Double> percentageMap = textAnalyze.getByPercentageOfVowelLetter();
        return percentageMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(toMap(e -> e.getKey(), e -> e.getValue(),
                        (e1, e2) -> e2, LinkedHashMap::new));
    }

}
